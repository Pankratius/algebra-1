\chapter{Further Remarks - Modules}
\section{Projective Modules}
\begin{defn}
  Let $M$ be an $R$-module. We say $M$ is a \emph{projective $R$-module} \index{module!projective} if for all epimorphisms $f:X\to Y$ and morphisms
  $p: M\to Y$ there is a morphism $f':M\to X$ such that the diagram
  \[
  \begin{tikzcd}
    &
    M\ar{d}[right]{p}\ar[dashed]{ld}[above left]{f'}&
    \\
    X\ar{r}&
    Y\ar{r}&
    0
  \end{tikzcd}
  \]
  commutes.
\end{defn}
\begin{example}
\leavevmode
\begin{enumerate}
  \item Let $M$ be a free $R$-module. Then $M$ is projective: Let $(e_i)$ be a basis for $M$. For each epimorphisms $f:X\to Y$ and morphism $M\to Y$ we can choose for a basis element $e_i$ a preimage under $f$, say $x_i$. Then the assignment $e_i\mapsto x_i$ can be extended to an $R$-linear map $M\to X$, by the universal property of the free module.
  \item The $\zz$-module $\zz/2\zz$ is not projective: Consider the projection $\zz \surarr \zz/2\zz$. Now any lift of the identity
  \[
  \begin{tikzcd}
    &
    \zz/2\zz\ar[equal]{d}\ar[dashed]{dl}[above left]{?}\\
    \zz \ar{r}&
    \zz/2\zz
  \end{tikzcd}
  \]
  would necessarily be the zero-morphism (a morphism $f:\zz/2\zz\to \zz$ has to satisfy $2f(1)=f(0)=0$) but then the diagram is far from being commutative.
\end{enumerate}
\end{example}
\section{Tensor Products}
\begin{defn}
  Let $M,N,P$ be $R$-modules and $f:M\times M'\to P$ a map. We say $h$ is \emph{$R$-bilinear}\index{bilinear! map of $R$-modules} if for all $x\in M$, $y\in N$ the maps
  \begin{alignat*}{3}
  		&f(x,-)\colon \, &&N \to P, \quad &&y' \mapsto f(x,y') \\
  		&f(-,y)\colon \, &&M \to P, \quad &&x' \mapsto f(x',y)
  \end{alignat*}
  are $R$-linear.
\end{defn}
\begin{prop}[Existence of Tensor Products 1]\label{modules:tensor-1}
  Let $M,N$ be $R$-modules.
  \begin{enumerate}
  \item
    Then there exists an $R$-module $M\tensor_R N$ and an $R$-bilinear map $g:M\times N \to M\tensor_R N$ such that for all other $R$-bilinear maps $f:M\times N\to P$ there is a unique $R$-linear map $f':M\tensor_R N\to P$ such that the diagram
    \[
    \begin{tikzcd}
      M\times N\ar{r}[above]{f}\ar{d}[left]{d}&
      P\\
      M\tensor_R N\ar[dashed]{ur}[below right]{\exists! f'}&
    \end{tikzcd}
    \]
    commutes.
  \item The tensor product is unique in the following sense: If there is another $R$-module $T$ with an $R$-bilinear map $h:M\times N\to T$ and the property that every $R$-bilinear map $f:M\times N\to P$ factors uniquely over $T$, then there exists a unique isomorphism of $R$-modules $\lambda: M\tensor_R N\isomorphism T$ such that the diagram
  \[
  \begin{tikzcd}
    M\times N \ar{r}[above]{g}\ar{rd}[below left]{h}&
    M\tensor_R N\ar[dashed]{d}[right]{\exists !\lambda}\\
    &
    T
  \end{tikzcd}
    \]
    commutes.
    \end{enumerate}
\end{prop}
\begin{proof}
  This is completely analogous to the proof for vector spaces, which can be found in \cite[16.2]{schroerla}. A proof where \glqq vector space\grqq{} is replaced with \glqq module\grqq{} can be found in \cite[Prop. 2.23]{franzen}.
\end{proof}
There is another way of defining tensor products, c.f. \cite[]{brandenburg}:
\begin{prop}[Existence of Tensor Products 2]\label{modules:tensor-2}
Let $M,N$ be $R$-modules. Consider the bilinear-functor
    \[
    \bilin_R(M,N;-): \rmod\to \setc
    \]
    which sends an $R$-module $P$ to the set of $R$-bilinear maps $M\times N\to P$ and an $R$-linear map $f:P\to Q$ to the induced map
    \begin{eqnarray*}
      \bilin_R(M,N;P)&\longrightarrow&\bilin_R(M,N;Q)\\
      \left(M\times N\xrightarrow{h}P\right) & \longmapsto & \left(M\times N \xrightarrow{h}P\xrightarrow{f}Q\right).
    \end{eqnarray*}
    \par
    Then there exists a representing object $M\tensor_R N$ for $\bilin_R(M,N;-)$, i.e. a natural isomorphism $\bilin_R(M,N;-)\to \homr(M\times N,-)$.
\end{prop}
The notions of a tensor product from \cref{modules:tensor-1} and \cref{modules:tensor-2} are the same. We call the $R$-module $M\tensor_R N$ the \emph{tensor product of $M$ and $N$ over $R$}.\par
We will now use the Yoneda-Embedding (\cref{categories:yoneda-embedding}) to show several properties of the tensor product:
\begin{lem}\label{modules:tensor-functoriality}
  Let $M,N,M',N'$ be $R$-modules and let $f:M\to M'$, $g:N\to N'$ be $R$-linear maps. Then there is a unique $R$-linear map
  \begin{eqnarray*}
    M\tensor_R N&\longrightarrow & M'\tensor_R N'\\
    a\tensor b& \longmapsto & f(a)\tensor g(b).
  \end{eqnarray*}
\end{lem}
\begin{proof}
  The maps $f,g$ induce a natural transformation
  \begin{eqnarray*}
    \eta: \bilin_R(M',N';-)& \longrightarrow & \bilin_R(M,N;-)\\
    \eta_P:\left(M'\times N'\xrightarrow{h}P\right)&
    \longmapsto &
    \left(M\times N\xrightarrow{(f,g)}M'\times N'\xrightarrow{h}P\right),
  \end{eqnarray*}
  where the map $(f,g):M\times N\to M'\times N'$ is given by $(a,b)\mapsto \left(f(a),g(b)\right)$. This is indeed natural as the diagram
  \[
  \begin{tikzcd}
    \bilin_R(M',N';P)\ar{rr}[above]{\eta_P}\ar{d}[left]{g\circ-}&
    &
    \bilin_R(M,N;P)\ar{d}[right]{g\circ-}\\
    \bilin_R(M',N';P')\ar{rr}[below]{\eta_{P'}}&
    &
    \bilin_R(M,N;P')
  \end{tikzcd}
  \]
  commutes for all $R$-linear maps $P\xrightarrow{g}P'$.\par
  By \cref{modules:tensor-2} this corresponds to a natural transformation
  \[
  \hom_R(M'\tensor_R N',-) \to \hom_R(M\tensor_R N,-).
  \]
  Using the Yoneda embedding (\cref{categories:yoneda-embedding}, i)), this corresponds to a unique $R$-linear map $M\tensor_R N\to M'\tensor_R N'$.
\end{proof}
\begin{lem}
  Let $M,N$ be $R$-modules. Then there is a $R$-linear isomorphism $M\tensor_R N\isomorphism N\tensor_R M$.
\end{lem}
\begin{proof}
  There is a natural isomorphism
  \begin{eqnarray*}
    \eta: \bilin_R(M\times N,-)&\longrightarrow & \bilin_R(N\times M,-)\\
    \eta_P: \left(M\times N\xrightarrow{(f,g)}P\right)&
    \longmapsto&
    \left(N\times M\xrightarrow{(g,f)}P\right).
  \end{eqnarray*}
  So by the Yoneda embedding, this corresponds to a $R$-linear isomorphism $M\tensor_R N\isomorphism N\tensor_R M$.
  \end{proof}
\begin{prop}[Tensor-Hom-Adjunction]
  Let $N$ be an $R$-module.
  \begin{enumerate}
    \item Consider the assignment
    \[
    F:\rmod\to\rmod,~M\mapsto M\tensor_R N
    \]
    which sends an $R$-linear map $f:M\to M'$ to the induced $R$-linear map $F(f)\defined f\tensor \id_N:M\tensor_R N\to M'\tensor_R N$ from \cref{modules:tensor-functoriality}. Then this defines an additive functor $\rmod\to \rmod$.
    \item Denote by $G$ the covariant hom-functor
    \begin{eqnarray*}
      G:\rmod&\longrightarrow &\rmod\\
        P &\longmapsto & \hom_R(N,P)\\
        \left(P\xrightarrow{h}P'\right)&
        \longmapsto&
        \left(\hom_R(N,P)\xrightarrow{h_{\ast}}\hom_R(N,P')\right).
    \end{eqnarray*}
    Then for all $M,P\in \rmod$, there is well-defined map
    \begin{eqnarray*}
      \phi: \hom_R\left(M\tensor_R N,P\right)&
      \longrightarrow&
      \hom_R\left(
      M,\hom_R\left(N,P\right)\right)\\
      f&\longmapsto&
      \left(
      x\mapsto
      \left(
      y\mapsto f(x\tensor y)\right)
      \right).
    \end{eqnarray*}
    \item The pair $(F,G,\phi)$ is an adjunction:\par
     For all $M,P\in \rmod$, the map $\phi$ from ii) is an isomorphism of $R$-modules, natural in $M$ and $P$ in the sense that the diagrams
    \[
    \begin{tikzcd}
      \hom_R\left(M'\tensor_R N, P\right) \ar{r}\ar{d}[left]{\left(f\tensor \id\right)^{\ast}}&
      \hom_R\left(M', \hom_R\left(N,P\right)\right)\ar{d}[right]{f^{\ast}}\\
      \hom_R\left(M\tensor_R N, P\right) \ar{r}&
      \hom_R\left(M, \hom_R\left(N,P\right)\right)
    \end{tikzcd}
    \]
    and
    \[
    \begin{tikzcd}
      \hom_R\left(M\tensor_R N, P\right) \ar{r}\ar{d}[left]{g_{\ast}}&
      \hom_R\left(M, \hom_R\left(N,P\right)\right)\ar{d}[right]{\left(g_{\ast}\right)_{\ast}}\\
      \hom_R\left(M\tensor_R N, P'\right) \ar{r}&
      \hom_R\left(M, \hom_R\left(N,P'\right)\right)
    \end{tikzcd}
    \]
    commute for all $R$-linear maps $f:M\to M'$, $g:N\to N'$.
 \end{enumerate}
\end{prop}
\begin{proof}
  Consider the maps
  \begin{eqnarray*}
    \bilin_R\left(M,N;P\right)&
    \longleftrightarrow&
    \hom_R\left(
    M,\hom_R\left(N,P\right)\right)\\
    \left(
    M\times N\xrightarrow{f}P\right)
    &\longmapsto&
    \left(
    b\mapsto f\left(-,b\right)\right)\\
    \left(a\times b\mapsto \psi\left(b\right)\left(a\right)\right)&
    \longmapsfrom&
    \left(b\mapsto \left(M\xrightarrow{\psi}P\right)\right).
  \end{eqnarray*}
  Then these are well-defined natural bijections.
  So we get for all $M,N,P\in \rmod$ a natural isomorphism
  \[
  \bilin_R\left(M\times N,P\right)\cong
  \hom_R\left(M,\hom_R\left(N,P\right)\right).
  \]
  But since
  \[
  \bilin_R\left(M\times N,P\right) \cong
  \hom_R\left(M\tensor_R N, P\right)
  \]
  by the universal property of the tensor-product, the claim follows.
\end{proof}

\begin{prop}
  Let $N$ be an $R$-module and let
  \[
  \begin{tikzcd}
    0\ar{r}&
    M'\ar{r}[above]{f}&
    M\ar{r}[above]{g}&
    M''\ar{r}&
    0
  \end{tikzcd}
  \]
  be a short-exact sequence of $R$-modules. Then the sequence
  \[
  \begin{tikzcd}
    M'\ar{r}[above]{f\tensor \id}&
    M\ar{r}[above]{g\tensor \id}&
    M''\ar{r}&
    0
  \end{tikzcd}
  \]
  is again exact. So the tensor-product functor is right-exact.
\end{prop}
\begin{proof}
  This follows from the fact that the tensor product functor is left-adjoint.
\end{proof}
\begin{cor}
Let $M$ be an $R$-module and $I\sse R$ an ideal. Then $R/I\tensor_R M \cong M/IM$ as $R$-modules.
\end{cor}
\begin{proof}
  Consider the exact sequence
  \[
  \begin{tikzcd}
    0\ar{r}&
    I\ar[hookrightarrow]{r}&
    R\ar[twoheadrightarrow]{r}&
    R/IR
  \end{tikzcd}.
  \]
  Tensoring with $M$ gives the exact sequence
  \[
  \begin{tikzcd}
    I\tensor_R M\ar[hookrightarrow]{r}&
    R\tensor_R M\ar[twoheadrightarrow]{r}&
    R/IR\tensor_R M
  \end{tikzcd}.
  \]
  So $R/IR\tensor M\cong \coker \left(I\tensor M\to M,~i\tensor \mapsto im\right)$, which is precisley $M/IM$.
\end{proof}
\begin{example}\label{modules:tensor-not-exact}
  It is in general \emph{not} true that the tensor product functor is left-exact: For that, consider the injective map $\zz\to \zz,~x\mapsto 2x$. Then tensoring with $\zz/2\zz$ gives an induced morphism
  \begin{eqnarray*}
    \zz\tensor_\zz \zz/2\zz&
    \longrightarrow&
    \zz\tensor_\zz \zz\\
    x\tensor 1 &
    \longmapsto&
    2x\tensor 1.
  \end{eqnarray*}
  But now we have $2x\tensor 1 = x\tensor 2 = 0$, so the induced homomorphism is the zero-morphism, and in particular not injective. (Note that we have $\zz\tensor_\zz \zz/2\zz \cong \zz/2\zz$).
\end{example}
\subsection{Flat Modules}
We saw in \cref{modules:tensor-not-exact} that the tensor product functor is in general not exact. This leads to the following definition:
\begin{defn}
  An $R$-module $N$ is \emph{flat} if the tensor-product functor $-\tensor_R N$ is exact.
\end{defn}
\begin{example}
  \leavevmode
  \begin{enumerate}
    \item The ring $R$ as a module is always flat, since $M\tensor_R R\cong N$ for all $R$-modules $N$.
    \item More generally, any projective $R$-module $P$ is flat. If $P$ is finitely presented, then the converse is also true.
  \end{enumerate}
\end{example}
\subsection{Extension of Scalars}
Let $\pphi:R\to R'$ be a ring homomorphism and $M$ an $R$-module. Then $\pphi$ induces the structure of an $R$-module on $R'$ and hence we can form the tensor product $R'\tensor_R M$. Now this induces the structure of an $R'$-module on $R'\tensor_RM$ given by
\[
r'_1\left(r'_2\tensor m\right) \defined \left(r'_1r_2'\right)\tensor m,
\]
which can be extended linearly. This is called \emph{extensions of scalars} or \emph{base change}.
\begin{prop}
  \leavevmode
  \begin{enumerate}
    \item Extension of scalars is functorial: For every $R$-module homomorphism $f:M\to M'$ there is an induced $R'$-linear morphism $R'\tensor_R M\to R'\tensor_R M'$. Denote this functor by $F:\rmod\to \rrmod$.
    \item Recall that by \cref{4: modules examples}, we can regard every $R'$-module as an $R$-module. This defines a functor $G:\rrmod\to \rmod$.
    \item The pair
    $\begin{tikzcd}[cramped, sep = small] F:\rmod\ar[shift left]{r}&\rrmod:G\ar[shift left]{l}\end{tikzcd}$ defines an adjunction.
  \end{enumerate}
\end{prop}
\begin{proof}
  \leavevmode
  \begin{enumerate}
    \item For every $R$-linear map $f:M\to M'$, we get an $R$-bilinear map
    \begin{eqnarray*}
      R'\times M & \longrightarrow & R'\times M'\\
      r',m & \longmapsto & r'\tensor f(m).
    \end{eqnarray*}
    This induces an $R$-linear map $R'\tensor_R M\to R'\tensor M'$, given on elementary tensors by $r'\otimes m\mapsto r'\otimes f(m)$. Cleary, this map is also $R'$-linear.
    \item An $R'$-linear map $M\to M'$ is in particular $R$-linear, since the action of $R$ on $M$ is given by $r.m\defined \pphi(r)m$.
    \item The assignment
    \begin{eqnarray*}
      \homrr(M\tensor_RR',M') & \longrightarrow & \homr(M,M')\\
      M\tensor_R R'\xrightarrow{f}M' & \longmapsto & (M\xrightarrow{f'}M',~m\mapsto f(1\tensor m))
    \end{eqnarray*}
    is a natural bijection.
  \end{enumerate}
\end{proof}

\section{Localization of Modules}\label{modapp:localization}
\begin{prop}
  Let $M$ be an $R$-module, $S\sse R$ a multiplicative set. Then the map
  \begin{eqnarray*}
  \sloc M & \isomorphism & \sloc R \tensor_R M\\
    \frac{x}{r} & \longmapsto & \frac{1}{r}\tensor x
  \end{eqnarray*}
  is an isomorphism of $\sloc R$-modules.
\end{prop}






\section{Local-Global}
For more, the reader is refered to \cite[00EN]{stacks-project}.
\begin{prop}[being zero is local]
  Let $M$ be an $R$-module. Then the following are equivalent:
  \begin{enumerate}
    \item $M=0$.
    \item $M_{\idp} = 0$ for all prime ideals $\idp \sse R$.
    \item $M_{\idm} = 0 $ for all maximal ideal $\idm \sse R$.
  \end{enumerate}
\end{prop}
\begin{prop}[being injective/surjective is local]
  Let $f:M\to N$ be an $R$-linear map.
  \begin{enumerate}
    \item The following are equivalent:
    \begin{enumerate}[label = \alph*)]
      \item $f$ is injective.
      \item $f_{\idp}: M_{\idp}\to N_{\idp}$ is injective for all prime ideals $\idp \sse R$.
      \item $f_{\idm}: M_{\idm}\to N_{\idm}$ is injective for all maximal ideals $\idm \sse R$.
    \end{enumerate}
    \item The following are equivalent:
    \begin{enumerate}[label = \alph*)]
      \item $f$ is surjective.
      \item $f_{\idp}: M_{\idp}\to N_{\idp}$ is surjective for all prime ideals $\idp \sse R$.
      \item $f_{\idm}: M_{\idm}\to N_{\idm}$ is surjective for all maximal ideals $\idm \sse R$.
    \end{enumerate}
  \end{enumerate}
\end{prop}

\section{Structure Theorems for Modules}
\subsection{Modules over PIDs}
\begin{defn}
  Let $R$ be a ring. We say $R$ is a \emph{principal ideal domain}\index{ring! PID} if $R$ is an integral domain and every ideal $I\sse R$ is of the form $\genby{a}$ for an $a\in R$.
\end{defn}
The following facts about modules over PIDs are taken from \cite[Chapter 4]{franzen}
\begin{prop}
  Let $R$ be a PID and $F$ a free $R$-module and $M\sse F$ a submodule. Then $M$ is free.
\end{prop}
\begin{theorem}
  Let $R$ be a PID and $M$ a finitely generated $R$-module. Then there is a $r\geq 0$, an $r\geq 0$ and prime elements $p_1,\hdots,p_n\in R$ such that for all $1\leq i\leq n$ there are $1\leq s_{i_1}\leq \hdots \leq s_{i_{t_i}}$ with
  \[
  M \cong
  R^r \bigoplus
  \left(
  \bigoplus_{i=1}^n
  \bigoplus_{j=1}^{l_i} A/\left(
  p_i^{s_{i_j}}\right)
  \right)
  \]
\end{theorem}
