\section{Zero Sets and Varieties}

\begin{defn}
\leavevmode
\begin{enumerate}
  \item Let $k$ be a field and $n\geq 1$ an integer. We denote by $\aspace_k^n\defined k^n$ the \emph{affine space}\index{affine!space}\index{space!affine}.
  \item Let $S\sse k[\polvariable_1,\hdots,\polvariable_n]$ be a subset. We denote by
  \[
  V(S)\defined
  \lset a \in \aspace_k^n \ssp f(a) = 0\text{ for all }f\in S\rset,
  \]
  the \emph{vanishing set of $S$}.
  Here, $f(a) = f\left((a_1,\hdots,a_n)\right)$ is a short-hand notation for the image of $f$ under the evaluation morphism $X_i \mapsto a_i$.
  \item Subsets of $\aspace_k^n$, which are of the form $V(T)$ for a subset $T\sse \ktn$, are called \emph{varieties} or \emph{algebraic subsets}\index{variety}\index{algebraic subset}.
\end{enumerate}
\end{defn}
\begin{example}
  These are some examples of varieties:
  \begin{enumerate}
    \item $V(x^2+y^2-1) = \lset (a,b)\in k^2\ssp a^2+b^2=1\rset$.
    \item $V(x\cdot y) = \lset (a,b)\in k^2\ssp a=0\text{ or }b=0\rset$.
    \item $V(\lset x-a,y-b\rset) = \lset (a,b)\rset$.
  \end{enumerate}
\end{example}
\begin{lem}\label{lec12:var-basic-props}
\leavevmode
\begin{enumerate}
  \item Let $S_1\sse S_2 \sse \ktn$. Then $V(S_1)\supseteq V(S_2)$.
  \item Let $S\sse \ktn$. Then $V(S) = V(\genby{S})$.
\end{enumerate}
\end{lem}
\begin{proof}
  Ommited.
\end{proof}

\begin{example}\label{lec12:zariski-closed-in-cc}
    We want to describe the varieties in $\aspace_k^1$ for $k$ algebraically closed. Let $I\sse k[\polvariable]$ be an ideal. Since $k[\polvariable]$ is a prinicipal ideal domain, there is a $f\in k[\polvariable]$ such that $I = \genby{f}$. Then, using \cref{lec12:var-basic-props} we get
    \begin{align*}
      V(I) &= V\left(\lset f\rset \right)\\
           &= \lset x \in k \ssp f(x) = 0\rset \\
           &= \lset x\in k \ssp \left(\polvariable-a_1\right)\cdot \hdots \cdot \left(\polvariable-a_n\right)= 0\rset\\
           &= \lset a_1,\hdots, a_n\rset.
    \end{align*}
    Note that it was crucial that $k$ is algebraically closed, since otherwise we would have not obtained a factorisation of $f$.
\end{example}
\begin{defn}
  \leavevmode
  \begin{enumerate}
    \item Let $X \sse \aspace_k^n$ be a subset. Define
    \[
    \iop(X)\defined \lset f\in \ktn\ssp f(x)=0\text{ for all }x\in X\rset.
    \]
    \item Let $X\sse \aspace_k^n$ be a subset. Define $A(X)\defined \ktn/\iop(X)$ as the \emph{ring of polynomial functions on $X$} or the \emph{coordinate ring at $X$}\index{coordinate ring}\index{ring! of polynomial functions}.
  \end{enumerate}
  Note that $A(X)$ is a finitely-generated $k$-algebra.
\end{defn}
\begin{proof}
  $\iop(X)$ is indeed an ideal, so this is well-defined.
\end{proof}
\begin{mrem}
  Note that $A\left(\aspace_k^n\right) = \ktn$.
\end{mrem}
\begin{example}
  Let $a\defined (a_1,\hdots,a_n)\in \aspace_k^n$. Then
  \[
  \iop(a) = m_a = \left(\polvariable_1-a_1,\hdots,\polvariable_n-a_n\right).
  \]
\end{example}
\begin{defn}
  Let $Y\sse X\sse \aspace_k^n$ and $S\sse A(X)$.
  \begin{enumerate}
    \item The set $V_X(S)\defined \lset x\in X\ssp f(x)=0\text{ for all }f\in S\rset$ is a \emph{subvariety of $X$}\index{subvariety}.
    \item We define $\iop_X(Y)\defined \lset f\in A(X)\ssp f(y) = 0 \text{ for all } y\in Y\rset.$
  \end{enumerate}
\end{defn}
\begin{lem}\label{lec12:subvar-prop}
  Let $X\sse \aspace_k^n$ be a variety, $Y,Y'\sse X$ and $S,S'\sse A(X)$.
  \begin{enumerate}
    \item If $Y\sse Y'$, then $\iop_X(Y)\supseteq \iop_X(Y')$. If $S\subset S'$, then $V_X(S)\supseteq V_X(S')$.
    \item It holds that $Y\sse V_X\left(\iop_X(Y)\right)$ and $S\sse \iop_X(V_X(S))$.
    \item If $Y$ is a subvariety of $X$, then $Y = V_X(\iop_X(Y))$.
    \item If $Y$ is a subvariety of $X$, then $A(X)/\iop_X(Y) = A(Y)$.
    \item It holds that $\iop_X(Y_1\cup Y_2)  = \iop_X(Y_1)\cap\iop(Y_2)$.
  \end{enumerate}
\end{lem}
\begin{proof}
  i), ii) and v) are clear from the definition. For iii), note that we only need to show $V_X(\iop_X(Y))\sse Y$, since the other inclusion follows from ii). But since $Y$ is a subvariety of $X$, there is a $S\sse A(X)$ such that $Y = V_X(S)$. Then, using i) and ii), we get
  \[
  S\overset{\text{ii)}}{\sse}\iop_X(V_X(S)) = I(Y)\overset{\text{i)}}{\implies} V_X(\iop_X(Y))\sse V_X(S) = Y.
  \]\par
  For iv), note that the restricion map
  \begin{eqnarray*}
    A(X)&\longrightarrow&A(Y)\\
    f&\longmapsto&\restrict{f}{Y}
  \end{eqnarray*}
  is well-defined and surjective, with kernel $\iop_X(Y)$.
\end{proof}
\begin{notation}
  In the following, if the subset $X$ is clear, we will ommit it in the notation for $\iop$ and $V.$
\end{notation}
\begin{rem}
  In light of \cref{lec12:subvar-prop} it seems reasonable to ask if $\iop(V(J))\sse J$ holds for a general ideal. But in general, this is far from being true:
  \begin{enumerate}
    \item For the ideal
    \[
    J\defined
    \genby{\left(\polvariable-a_1\right)^{k_1}\cdot \hdots \cdot \left(\polvariable-a_n\right)^{k_n}}\sse \cc[\polvariable]
    \]
    with $k_i\geq 0$ and $a_i\in \cc$, we have $V(J) = \lset a_1,\hdots,a_n\rset$. So $\iop(V(J))$ consists of all polynomials which have each of the factors $\polvariable-a_i$ at least once. So if one of the $k_i$ is greater than 1 $J$ is a proper subset of $\iop(V(J))$.
    \item Consider
    \[
    J \defined \genby{\polvariable^2+1}\sse \rr[\polvariable].
    \]
    Then $\iop(J) = \emptyset$, so $\iop(V(J)) = \iop(\emptyset) = \rr[\polvariable]$.
  \end{enumerate}
\end{rem}

\lec
