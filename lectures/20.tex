\begin{lem}\label{lec20:valuation-ring-basic-props}
  Let $R = R_{\nu}$ be a valuation ring.
  \begin{enumerate}
    \item The ring $R$ is an integral domain.
    \item For all $a\in K\unts$ it holds that $a\in R$ or $a^{-1}\in R$.
    \item For all $a,b\in R$ it holds that $\nu(a)\leq \nu(b)$ if and only if $b\in \genby{a}$.
    \item The group of units of $R$ is given by $R\unts = \ker \nu$.
    \item It holds that $R$ is a local ring, with unique maximal ideal
    \[\idm = \lset a\in R\ssp \nu(a) > 0\rset.\]
    \item The ring $R$ is normal.
  \end{enumerate}
\end{lem}

\begin{proof}
  \leavevmode
  \begin{enumerate}
    \item Since $R$ is a subring of a field, it is an integral domain.
    \item Since $\nu$ is a group homomorphism, we have
    \[\nu(a)+\nu(a^{-1}) = \nu(1) = 0.\] So $\nu(a)\geq 0$ or $\nu(a^{-1}) \geq 0$.
    \item Assume that $a = 0$. Then $\nu(b)\geq \infty$ if and only if $\nu(b)=\infty$ if and only if $b=0$ if and only if $b\in \genby{0}$, since $R$ is an integral domain.\par
    So assume $a\neq 0$. Then $\nu(a)\leq \nu(b)$ if and only if $\nu(b/a)\geq 0$ if and only if $b/a\in R$ if and only if $b\in \genby{a}$.
    \item Let $a\in R\setminus \lset 0\rset$. Then $a\in R\unts$ if and only if $a^{-1}\in R$ if and only if $\nu(a^{-1}) = -\nu(a)\geq 0$ if and only if $\nu(a) = 0$.
    \item Since $\nu$ is a group homomorphism, $\idm$ is an ideal. Let now $I\sse R$ be an ideal such that $I\not \sse\idm$. Then $I$ contains an element $a\in R$ such that $\nu(a)=0$, and hence $I = \genby{1}$, by iv).
    \item The fraction field of $R$ is given by $K$. Let now $a\in K\unts$ be integral over $R$. So there are $c_{n-1},\hdots,c_0$ such that
    \[
    a^n+c_{n-1}a^{n-1}+\hdots+c_0 = 0.
    \]
    Assume $a\notin R$. Then $a^{-1}\in R$ by ii). Hence
    \[
    a = -\left(c_{n-1}+c_{n-2}a^{-1}+\hdots+a^{n-1}c_0\right),
    \]
    and all summands are in $R$. But this is a contradiction.
  \end{enumerate}
\end{proof}

\begin{prop}
  For a ring $R$, the following are equivalent:
  \begin{enumerate}
    \item $R$ is a valuation ring.
    \item $R$ is an integral domain such that for all $a\in \left(\quot R\right)\unts$ it holds that $a\in R$ or $a^{-1}\in R$.
  \end{enumerate}
\end{prop}
\begin{proof}
  The direction i) $\implies$ ii) follows from \cref{lec20:valuation-ring-basic-props}. For the other direction, set $K\defined \quot R$, $G\defined K\unts/R\unts$ (as quotient of abelian groups) and denote by $\overline{a}$ the image of $a\in K\unts$ in $G$. We make $G$ into a totally ordered abelian group by setting:
  \[
  \overline{a} \leq \overline{b} \text{ if and only if }b/a\in R.
  \]
  Then this is well-defined, since for units $c_1,c_2\in R\unts$, we have $a/b\in R$ if and only if $c_1/c_2\cdot a/b\in R$. It is antisymmetric, since $a/b\in R$ and $b/a\in R$ implies that there is a unit $c\in R\unts$ such that $a/b = c$, transitive and by assumption $\overline{a}\leq \overline{b}$ or $\overline{b}\leq \overline{a}$ always holds. The relation is also compatible with the group structure on $G$, since $b/a\in R$ implies $bc/ac\in R$ for all $c\in K\unts$.\par
  Denote by $\nu:K\unts \to G$ the quotient map. Then $\nu(a)\leq \nu(b)$ if and only if $b/a\in R$.
  Then $\nu$ is indeed a valuation on $K\unts$: By construction, $\nu$ is a group homomorphism, and if $a/b\in R$ then $(a+b)/b = a/b+1\in R$, and hence $\nu(a+b)\geq \nu(b)$ follows.
\end{proof}
\begin{rem}
  Let $R_{\nu}\sse K$ be a valuation ring for the valuation $\nu:K\unts \to G$. Then this already determines the map $\nu$ in the following sense: It holds that $K = \quot R$, $\nu\left(K\unts\right) = K\unts/R\unts$ and $\nu(a)\leq \nu(b)$ if and only if $\nu(b/a)\leq 0$ if and only $b/a\in R$.
\end{rem}
\begin{mrem}\label{lec20:valuation-fg-implies-principal}
  Let $R$ be a valuation ring. Then every finitely-generated ideal is already principal.
\end{mrem}
\begin{proof}
  Let $I=\genby{a_1,\hdots,a_n}\sse R$ be a finitely-generated ideal. Then there is a $1\leq i\leq n$ such that $\nu(a_i)\leq \nu(a_k)$ for all $1\leq k\leq n$. Hence by \cref{lec20:valuation-ring-basic-props} iii) it follows that $a_k\in \genby{a_i}$ and hence $I = \genby{a_i}$.
\end{proof}

\section{Discrete Valuation Rings}
\begin{prop}\label{lec20:dvr-def-lem}
  Let $R$ be a valuation ring with maximal ideal $\idm$. Then the following are equivalent:
  \begin{enumerate}
    \item $R$ is noetherian but not a field.
    \item $R$ is a principal ideal domain but not a field.
    \item The value group of $R$ is given by $\zz$.
  \end{enumerate}
\end{prop}
\begin{defn}
  In this case, we say that $R$ is a \emph{discrete valuation ring}\index{valuation ring!discrete}.
\end{defn}
\begin{proof}
The direction i) $\implies$ ii) follows directly from \cref{lec20:valuation-fg-implies-principal} and the direction ii) $\implies$ i) is trivial.
For ii) $\implies$ iii) we first note that there is an element $p\in R$ that generates $\idm$. Then for the ideal $I\defined \cup_{n\geq 1}^{\infty}\genby{p^n}$ there is also an element $y\in R$ such that $I = \genby{y}$.
So in particular $y = pz$ for a $z\in R$ and $z\in \genby{p^n}$ for all $n\geq 1$, i.e. $z\in I$.
Hence we can write $z = uy$ for another element $u\in R$. Now $y = pz = puy$ implies $y\neq 0$ since $p$ is not a unit. So for every $a\in R$ there is a well-defined integer $\nu(a)$ such that $a\in \genby{p^{\nu(a)}}$ but $a\notin\genby{p^{\nu(a)+1}}$ and $\nu(a)$.
We can extend this map to $\quot R$ to get a valuation with value group $\zz$.\par
Now for iii) $\implies$ ii) we choose a $t\in R$ such that $\nu(t)= 1$. Then for every $x\in \idm$ there is a $n>0$ such that $\nu(x/t^n) = 0$ and hence $x = t^nu$ for a unit $u\in R\unts$. So in particular $\idm = \genby{b}$. Now for every non-zero ideal $I\sse R$ we consider the set
\[
\lset
  \nu(a)\ssp a\in I\setminus\lset 0\rset \rset .
\]
Then this contains a minimal element, say $k$. If $k = 0$ then $I$ contains a unit (by \cref{lec20:valuation-ring-basic-props}) and hence $I  = \genby{1}$. If $\nu(a)=k>0$ then $I = \genby{a}$ follows.
  \end{proof}

\begin{prop}\label{lec20:dvr-alt-char-ii}
  Let $R$ be a local noetherian ring but not a field. Then the following are equivalent:
  \begin{enumerate}
    \item $R$ is a discrete valuation ring.
    \item $R$ is a prinicipal ideal domain but not a field.
    \item $R$ is a one-dimensional factorial ring.
    \item $R$ is a one-dimensional normal and local domaincc.
    \item $R$ is a one-dimensional regular local ring.
  \end{enumerate}
\end{prop}
\begin{proof}
  The direction i) $\implies$ ii) is just \cref{lec20:dvr-def-lem} above.
  Now ii) $\implies$ iii) is immediate and iii) $\implies$ iv) follows from the fact that factorial rings are always noral. \par
  For iv) $\implies$ v) we first note that since $R$ is local, a domain and one-dimensional we have $\spec R  = \lset \genby{0}, \idm\rset$ and hence  $\idm$ is trivialy an ideal of definition for all non-zero $a\in \idm$.
  So (\cref{lec16:defintionideal}) there is a $n\geq 1$ such that $\idm^n\sse \genby{a}$.
  Choose $n$ minimal with this property.
  Let $b\in \idm^{n-1}\setminus$ and set $p\defined a/b\in \quot R$.
  \begin{claim}
    We have $p\in R$ and $\idm = \genby{p}$.
  \end{claim}
  \begin{claimproof}
    We have
    \[
    \frac{1}{p}\idm = \frac{b}{a}\idm \sse \frac{1}{a}\idm^n \sse \frac{1}{a}\genby{a}\sse R
    \]
    so we can indeed identify $1/p\cdot \idm$ with an ideal in $R$. Assume now for the moment that $1/p\cdot \idm \sse \idm$. Then the map
    \begin{eqnarray*}
      f:\idm & \longrightarrow & \idm \\
      x & \longmapsto & 1/p\cdot x
    \end{eqnarray*}
    is well-defined and $R$-linear. By Cayley-Hamilton (\cref{5:cayley-hamilton}) there is a monic polynomial $p = \sum a_i \polvariable^i\in R[\polvariable]$ such that $p(f)=0$. Hence for all $x\in \idm \sse K$ it holds that
    \[
    \sum a_i \left(1/p\right)^i x = 0
    \]
    and hence $1/p$ is integral over $R$ (since $K$ is a field the above equation becomes an integral equation for $1/p$).
    Now since $R$ is normal this implies that $1/p\in R$ and hence $n = 1/p a \in \genby{a}$, contradicting $b\notin \genby{a}$. Hence $\frac{1}{p}\idm = R$ and so $\idm = \genby{p}$.
    So $R$ is local.\par
    Finally, for v) $\implies$ i) we have $\idm = \genby{p}$ for an element $p\in R$. By \cref{lec19:dvr-power-in-maximal} there is a unit $e\in R\unts$ and a unique $n\geq 1$ such that $a = e p^n$.
    So in $\quot R$ every element can be written as $dp^m$ with $m\in \zz$ and $d\in R\unts$. This implies that $R$ is a valuation ring, by \cref{lec20:valuation-ring-basic-props} ii). By the above \cref{lec20:dvr-def-lem}.

  \end{claimproof}
\end{proof}


\lec
